#pragma once

class MyClass
{
public:
	MyClass() = default;
	~MyClass() = default;

	static int multiplyByTwo( int n );

	static int multiplyByThree( int n );

	static int multiplyByTwoIfPair( int n );
};
