project(cmake-test-coverage CXX)
cmake_minimum_required(VERSION 2.8.9)

set(CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/cmake" "${CMAKE_PREFIX_PATH}")

set(SOURCE_DIR "${CMAKE_SOURCE_DIR}/src")
include_directories("${CMAKE_SOURCE_DIR}/include")

add_definitions(
	-std=c++11
	-fno-permissive
	-pedantic
	-Wall
	-Wextra
)

add_subdirectory("src")

# Enable testing
enable_testing()
add_subdirectory(tests)

# Enable coverage report
# TARGETS defines the targets we analyse 
# FILTER defines the parts that should not be included in the coverage report (tests dir itself)
# TESTS defines the tests that are going to be executed to create the report
find_package(CoverageReport)
enable_coverage_report(
  TARGETS
    unit-tests
	my_class_static
  FILTER
    ${CMAKE_SOURCE_DIR}/tests/*
    ${CMAKE_BINARY_DIR}/*
  TESTS
    unit-tests
)
