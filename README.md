![Build Status](https://gitlab.com/xavi-garcia-mena/test-coverage/badges/master/build.svg)

# Test coverage example with cmake, gcovr and lcov
## Install dependencies
In order to run this code you'll need the following dependencies:
* gcovr
* lcov
* boost test libraries
* C++ compiler with C++11 support.

### Ubuntu 16.04 or later system:
```bash
sudo apt-get install libboost-test-dev gcovr lcov build-essential
``` 

### CentOS 6:
```bash
sudo yum -y install centos-release-scl epel-release
sudo yum -y install devtoolset-6-gcc-c++ cmake boost-devel python-pip lcov
sudo pip install gcovr
# enable devtoolset-6
source /opt/rh/devtoolset-6/enable
```

## Build
```bash
mkdir buildcoverage
cd buildcoverage
cmake -DCMAKE_BUILD_TYPE=coverage ..
make
make test
make coverage
```
After the above commands you'll have your coverage report under the **coveragereport** folder in your build directory.

You can open the index.html file which should look similar to:
![Coverage Index](https://gitlab.com/xavi-garcia-mena/test-coverage/raw/master/images/index.png)

If you click on src, you'll see all the source files and their testing coverage:
![Coverage Src](https://gitlab.com/xavi-garcia-mena/test-coverage/raw/master/images/src.png)

And finally, if you click on **my_class.cpp** you'll see the coverage report for that file:
![Coverage Index](https://gitlab.com/xavi-garcia-mena/test-coverage/raw/master/images/my_class_cpp.png)

**Lines not covered by your tests are highlighted in red.**

## Port to your project
In order to port the coverage report to your project you should copy all the files contained in the **cmake** directory.

You should also add the following to your CMakeLists.txt file:
```cmake
set(CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/cmake" "${CMAKE_PREFIX_PATH}")

#
# DEFINE YOUR TARGETS AND CONFIGURATION HERE
#

# Enable coverage report
# TARGETS defines the targets we analyse
# FILTER defines the parts that should not be included in the coverage report (tests dir itself)              
# TESTS defines the tests that are going to be executed to create the report
find_package(CoverageReport)
enable_coverage_report(
    TARGETS
      your-unit-tests-target
      your-target-with-the-code-to-be-analysed
    FILTER
      ${CMAKE_SOURCE_DIR}/tests/* # change to match your tests directory
      ${CMAKE_BINARY_DIR}/*
    TESTS
      your-unit-tests-target
) 
```
## Credits
The cmake files used in this example are part of the [cmake-extras](https://github.com/unity8-team/cmake-extras) Ubuntu package.
