#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Suites
#include <boost/test/unit_test.hpp>

#include "my_class.h"

BOOST_AUTO_TEST_SUITE(TestMyClass)

BOOST_AUTO_TEST_CASE(MultiplyByTwo)
{
	BOOST_CHECK(4 == MyClass::multiplyByTwo(2));
}

BOOST_AUTO_TEST_CASE(MultiplyByTwoIfPair)
{
	BOOST_CHECK(-1 == MyClass::multiplyByTwoIfPair(3));
}

BOOST_AUTO_TEST_SUITE_END()
